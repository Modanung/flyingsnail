# De slak die vloog

-----

### 1)

Op een dag in het veld  
waren er welgeteld  
vijf slakken  
tesamen verzeild

Hun buikjes vol  
met sla en kool  
misten zij enkel nog lol

-----

### 2)

Één kreeg een idee:  
"Wie doet er mee  
met een race naar de hofstee?"

De andere vier  
verveeld als een stier  
Spraken in koor:
"Dan beginnen we hier!"

-----

### 3)

De slakken bewogen  
naar ieders vermogen  
haast tot ze ruimtetijd verbogen

Één raakte wat achter  
en de voorste die lachtte  
vanwege de winst die hij verwachtte

-----

### 4)

Toen verscheen  
uit het wazig ver heen  
twee reusachtige been'

De slak in de achterhoede  
werd zonder vermoeden  
opgetild, en zei:  
"Mijn goede!"

-----

### 5)

Wat krijgen we nou?  
Laat ik me gauw  
terugtrekken in mijn ruggebouw

Toen gooide de reus  
krachtig, ja heus  
de slak had geen ene keus!

-----

### 6)

Het slakje vloog  
met grote boog  
door de lucht, oh zo hoog!

Tijdens zijn vlucht  
dacht hij verzucht:  
"Vind ik mijn huis  
straks nog wel terug?"

(Ohja, die zit op mijn rug)

-----

### 7)

Ietwat terstond  
was daar de grond  
Als de slak  
zich maar niet verwondt!

Gelukkig landde  
hij in zachte handen:  
In het mos  
is waar hij verzandde

-----

### 8)

En warempel  
Daar was ook de drempel  
"Krijg ík dan nu de stempel?"

Compleet verbaasd  
en nog altijd gehaast  
kwam daar de rest aangeraast

-----

### 9)

"Dit kan niet,  
jij hebt valsgespeeld!"

"Ik had geen keus:  
Er was een reus,  
die heeft mij opgetild."

-----

### 10)

De _race_ was plots vergeten:  
Alles over deze _reus_  
wilden zij weten

Maar ze hadden ook  
al weer honger gekregen



